INSERT INTO clientes (id, nome,cep,logradouro,complemento,bairro,localidade,uf,latitude,longitude) VALUES (1, 'Goku da Silva','90130-080', 'Nunes Machado', 'APTO 107', 'Azenha', 'Porto Alegre', 'RS', 0.00, 0.00);

INSERT INTO usuarios (id, name,email,password) VALUES (1, 'Goku da Silva','goku@gmail.com', '$2a$10$sADew18yFLbdHCpIvVtn/uY/UKlhY0oZmUERefu92SJy0zSQrf0LS');


INSERT INTO lojas (id, cep, logradouro, complemento, bairro, localidade, uf, latitude, longitude) VALUES
    (1, '90130-080', 'Nunes Machado', 'APTO 107', 'Azenha', 'Porto Alegre', 'RS', 0.00, 0.00);


INSERT INTO produtos (id, nome, valor, desconto, quantidade) VALUES
    (1, 'Creme Dental Oral-b Extra Fresh 70g Pack C/ 3 Unidades', 6.99, 0.18, 2);

INSERT INTO motoboys (id, nome, telefone, status,latitude, longitude) VALUES
    (1, 'Naruto da Silva','(51)9815-7898','EM VIAGEM', -30.047393111706484, -51.17228063960943);


INSERT INTO pedidos (id, pedido_status, loja_id, cliente_id, motoboy_id) VALUES
    (1, 2, 1,1,1);

INSERT INTO produto_pedido (produto_id, pedido_id) VALUES (1,1);

INSERT INTO clientes (id, nome,cep,logradouro,complemento,bairro,localidade,uf,latitude,longitude) VALUES (2, 'Goku da Silva','90130-080', 'Nunes Machado', 'APTO 107', 'Azenha', 'Porto Alegre', 'RS', 0.00, 0.00);

INSERT INTO lojas (id, cep, logradouro, complemento, bairro, localidade, uf, latitude, longitude) VALUES
    (2, '90130-080', 'Nunes Machado', 'APTO 107', 'Azenha', 'Porto Alegre', 'RS', 0.00, 0.00);


INSERT INTO produtos (id, nome, valor, desconto, quantidade) VALUES
    (2, 'Creme Dental Oral-b Extra Fresh 70g Pack C/ 3 Unidades', 6.99, 0.18, 2);

INSERT INTO motoboys (id, nome, telefone, status,latitude, longitude) VALUES
    (2, 'Naruto da Silva','(51)9815-7898','DISPONIVEL', -30.047393111706484, -51.17228063960943);


INSERT INTO pedidos (id, pedido_status, loja_id, cliente_id, motoboy_id) VALUES
    (2, 1 , 2,2,null);


INSERT INTO produto_pedido (produto_id, pedido_id) VALUES (2,2);

