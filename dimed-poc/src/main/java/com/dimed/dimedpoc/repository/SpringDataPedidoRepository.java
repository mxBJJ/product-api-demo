package com.dimed.dimedpoc.repository;

import com.dimed.dimedpoc.model.Pedido;
import com.dimed.dimedpoc.service.PedidoService;
import org.springframework.data.repository.Repository;


public interface SpringDataPedidoRepository extends PedidoRepository, Repository<Pedido, Integer> {
}
