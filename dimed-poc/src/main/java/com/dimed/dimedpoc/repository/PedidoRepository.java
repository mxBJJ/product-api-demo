package com.dimed.dimedpoc.repository;

import com.dimed.dimedpoc.model.Pedido;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface PedidoRepository {
    Collection<Pedido> findAll();
    void save(Pedido pedido);
}
