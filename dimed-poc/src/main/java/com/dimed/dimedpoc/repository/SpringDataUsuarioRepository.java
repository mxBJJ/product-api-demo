package com.dimed.dimedpoc.repository;

import com.dimed.dimedpoc.model.Usuario;
import org.springframework.data.repository.Repository;

public interface SpringDataUsuarioRepository extends UsuarioRepository, Repository<Usuario, Integer> {
}
