package com.dimed.dimedpoc.repository;

import com.dimed.dimedpoc.model.Usuario;

public interface UsuarioRepository {
    Usuario findByEmail(String email);
}
