package com.dimed.dimedpoc.service;

import com.dimed.dimedpoc.model.Pedido;
import org.springframework.dao.DataAccessException;

import java.util.Collection;

public interface PedidoService {

    Collection<Pedido> findAll() throws DataAccessException;
    void criarPedido(Pedido pedido) throws DataAccessException;

}
