package com.dimed.dimedpoc.service;

import com.dimed.dimedpoc.security.UserSS;
import org.springframework.security.core.context.SecurityContextHolder;

public class UsuarioService {

    public static UserSS authenticated() {
        try {
            return (UserSS) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }
        catch (Exception e) {
            return null;
        }
    }
}
