package com.dimed.dimedpoc.service;

import com.dimed.dimedpoc.model.Pedido;
import com.dimed.dimedpoc.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PedidoServiceImpl implements PedidoService{

    private PedidoRepository pedidoRepository;


    @Autowired
    public PedidoServiceImpl(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @Override
    public Collection<Pedido> findAll() throws DataAccessException {
        Collection<Pedido> pedidos = pedidoRepository.findAll();
        return pedidos;
    }

    @Override
    public void criarPedido(Pedido pedido) throws DataAccessException {
        pedidoRepository.save(pedido);
    }
}
