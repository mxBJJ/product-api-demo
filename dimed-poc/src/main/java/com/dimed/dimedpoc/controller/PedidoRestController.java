package com.dimed.dimedpoc.controller;

import com.dimed.dimedpoc.model.Pedido;
import com.dimed.dimedpoc.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;


@RestController
@RequestMapping("/pedidos")
public class PedidoRestController {

    @Autowired
    private PedidoService pedidoService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<Collection<Pedido>> findAllPedidos(){
        Collection<Pedido> pedidos = pedidoService.findAll();
        return new ResponseEntity<Collection<Pedido>>(pedidos, HttpStatus.OK);
    }


    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Void> insertPedido(@RequestBody Pedido pedido){
        pedidoService.criarPedido(pedido);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
}
