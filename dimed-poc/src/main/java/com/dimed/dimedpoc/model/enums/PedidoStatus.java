package com.dimed.dimedpoc.model.enums;

public enum PedidoStatus {

    RECEBIDO(1, "PEDIDO RECEBIDO"),
    COLETADO(2,"EM TRANSPORTE");

    private Integer cod;
    private String description;


    PedidoStatus(Integer cod, String description) {
        this.cod = cod;
        this.description = description;
    }

    public Integer getCod() {
        return cod;
    }

    public String getDescription() {
        return description;
    }
}
